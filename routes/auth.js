const router = require('express').Router();
const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const userValidation = require('../validations/userValidation')
const loginValidation = require('../validations/loginValidation')

router.post('/sign-up', async (req, res) => {
    const { error } = userValidation.validate(req.body)

    if (error) return res.status(422).send(error.details[0].message);

    const emailExists = await User.findOne({ email: req.body.email });

    if (emailExists) return res.status(400).send('Email alread exists');

    const password = await bcrypt.hash(req.body.password, 10);

    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password 
    });
    
    try {
        const { name, email, _id } = await user.save();
        res.send({ _id, name, email });
    } catch (error) {
        res.status(422).send(error);
    }
});

router.post('/login', async (req, res) => {
    const { error } = loginValidation.validate(req.body);
    if (error) return res.status(422).send(error.details[0].message);

    const user = await User.findOne({ email: req.body.email});
    if (!user) return res.status(400).send('Email doesn´t exists');
    
    const passwordIsCorrect = await bcrypt.compare(req.body.password, user.password);
    if (!passwordIsCorrect) return res.status(401).send('Your password is wrong');

    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET);
    res.header('authentication', token).send(token);
})

module.exports = router;