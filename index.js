const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongoose = require('mongoose');

// Routes
const AuthRoute = require('./routes/auth');

dotenv.config();

// DB connection
mongoose.connect(
    process.env.DB_CONNECTION_URI,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => console.log('Connected to DB')
);

// Middlewares
app.use(express.json());

// Route middlewares
app.use('/api/auth', AuthRoute);

app.listen(3000, () => console.log('Running'));